package transactions;

import com.n26.model.StatisticsModel;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertTrue;

public class StatisticsModelTest {

    @Test
    public void statisticsModelTest(){
        StatisticsModel statisticsModel = new StatisticsModel();

        assertTrue(statisticsModel.getSum().equals(new BigDecimal(0)));
        assertTrue(statisticsModel.getAvg().equals(new BigDecimal(0)));
        assertTrue(statisticsModel.getCount().equals(0L));
    }
}
