package transactions;

import com.google.common.collect.Lists;
import com.n26.model.FilteredTransactionModel;
import com.n26.model.StatisticsResponseModel;
import com.n26.model.TransactionModel;
import com.n26.model.TransactionModelRequest;
import com.n26.service.TransactionsServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TransactionsServiceTest {

    private TransactionsServiceImpl transactionsService;

    @Before
    public void setup(){ transactionsService = new TransactionsServiceImpl();}

    @Test
    public void createTransaction(){
        FilteredTransactionModel filteredTransactionModel = transactionsService.createTransaction(
                new TransactionModelRequest("1", "2018-07-17T09:59:51.312Z"));
        assertTrue(filteredTransactionModel.getOld());
    }

    @Test
    public void createOldTransaction(){
        FilteredTransactionModel filteredTransactionModel = transactionsService.createTransaction(
                new TransactionModelRequest("1", Instant.now().toString()));
        assertFalse(filteredTransactionModel.getOld());
    }

    @Test
    public void getStatisticsTransactionCreated(){
        transactionsService.createTransaction(new TransactionModelRequest("1", Instant.now().toString()));
        StatisticsResponseModel statisticsResponseModel = transactionsService.getStatistics();
        assertTrue(statisticsResponseModel.getSum().equals("1.00"));
    }

    @Test
    public void removeTransactionFromListTestNoAdd(){
        TransactionModel transactionModel = new TransactionModel();
        int transactionPosition = -1;
        List<TransactionModel> transactionModelList =Lists.newArrayList();
        transactionModelList = addTransactionToListMethod(transactionModel, transactionPosition, transactionModelList);

        assertTrue(transactionModelList.size() == 0);
    }

    @Test
    public void removeTransactionFromListTestAddElement(){
        TransactionModel transactionModel = new TransactionModel();
        int transactionPosition = 0;
        List<TransactionModel> transactionModelList =Lists.newArrayList();
        transactionModelList = addTransactionToListMethod(transactionModel, transactionPosition, transactionModelList);

        assertTrue(transactionModelList.size() == 1);
    }

    @Test
    public void removeTransactionFromListTestAddElementFirstPositionFullList(){
        TransactionModel transactionModel = new TransactionModel();
        TransactionModel transactionModel2 = new TransactionModel();
        TransactionModel transactionModel3 = new TransactionModel();
        int transactionPosition = 0;
        List<TransactionModel> transactionModelList =Lists.newArrayList();
        transactionModelList.add(transactionModel);
        transactionModelList.add(transactionModel2);
        transactionModelList = addTransactionToListMethod(transactionModel3, transactionPosition, transactionModelList);

        assertTrue(transactionModelList.size() == 1);
    }

    @Test
    public void setMinTransactionTest(){
        TransactionModel transactionModel = new TransactionModel(new BigDecimal(4), 1001L);
        TransactionModel transactionModel2 = new TransactionModel(new BigDecimal(3), 1002L);
        TransactionModel transactionModel3 = new TransactionModel(new BigDecimal(1), 1003L);

        List<TransactionModel> transactionModelList =Lists.newArrayList();
        transactionModelList.add(transactionModel);
        transactionModelList.add(transactionModel2);

        transactionModelList = setMinTransaction(transactionModelList, transactionModel3);

        assertTrue(transactionModelList.get(0).getAmount().equals(transactionModel3.getAmount()));

    }

    @Test
    public void setMaxTransactionTest(){
        TransactionModel transactionModel = new TransactionModel(new BigDecimal(1), 1001L);
        TransactionModel transactionModel2 = new TransactionModel(new BigDecimal(3), 1002L);
        TransactionModel transactionModel3 = new TransactionModel(new BigDecimal(4), 1003L);

        List<TransactionModel> transactionModelList =Lists.newArrayList();
        transactionModelList.add(transactionModel);
        transactionModelList.add(transactionModel2);

        transactionModelList = setMaxTransaction(transactionModelList, transactionModel3);

        assertTrue(transactionModelList.get(0).getAmount().equals(transactionModel3.getAmount()));

    }

    @Test
    public void removeMaxOrMinTransactionFromListTest(){
        TransactionModel transactionModel = new TransactionModel(new UUID(33L, 33L), new BigDecimal(1), 1001L);
        TransactionModel transactionModel2 = new TransactionModel(new UUID(35L, 21L),new BigDecimal(3), 1002L);
        TransactionModel transactionModel3 = new TransactionModel(new UUID(33L, 33L),new BigDecimal(4), 1003L);

        List<TransactionModel> transactionModelList =Lists.newArrayList();
        transactionModelList.add(transactionModel);
        transactionModelList.add(transactionModel2);

        removeMaxOrMinTransactionFromList(transactionModelList, transactionModel3);

        assertTrue(transactionModelList.get(0).getUuid().equals(transactionModel2.getUuid()));

    }

    private List<TransactionModel> addTransactionToListMethod(TransactionModel transactionModel, int transactionPositionToAdd,
                                                              List<TransactionModel> transactionList){
        try {
            Method method = transactionsService.getClass().getDeclaredMethod("addTransactionToList",
                    TransactionModel.class, int.class, List.class);
            method.setAccessible(true);

            return (List<TransactionModel>) method.invoke(transactionsService, transactionModel,
                    transactionPositionToAdd, transactionList);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return Lists.newArrayList();
    }

    private List<TransactionModel> setMinTransaction(List<TransactionModel> minList, TransactionModel transactionModel){
        try {
            Method method = transactionsService.getClass().getDeclaredMethod("setMinTransaction",
                    List.class, TransactionModel.class);
            method.setAccessible(true);

            return (List<TransactionModel>) method.invoke(transactionsService, minList, transactionModel);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return Lists.newArrayList();
    }

    private List<TransactionModel> setMaxTransaction(List<TransactionModel> maxList, TransactionModel transactionModel){
        try {
            Method method = transactionsService.getClass().getDeclaredMethod("setMaxTransaction",
                    List.class, TransactionModel.class);
            method.setAccessible(true);

            return (List<TransactionModel>) method.invoke(transactionsService, maxList, transactionModel);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return Lists.newArrayList();
    }

    private List<TransactionModel> removeMaxOrMinTransactionFromList(List<TransactionModel> transactionList, TransactionModel transactionModel){
        try {
            Method method = transactionsService.getClass().getDeclaredMethod("removeMaxOrMinTransactionFromList",
                    List.class, TransactionModel.class);
            method.setAccessible(true);

            return (List<TransactionModel>) method.invoke(transactionsService, transactionList, transactionModel);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return Lists.newArrayList();
    }
}
