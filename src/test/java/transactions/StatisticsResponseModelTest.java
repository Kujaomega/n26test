package transactions;

import com.n26.model.StatisticsModel;
import com.n26.model.StatisticsResponseModel;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class StatisticsResponseModelTest {

    @Test
    public void statisticsResponseModelTest(){
        StatisticsResponseModel statisticsResponseModel= new StatisticsResponseModel(new StatisticsModel());

        assertTrue(statisticsResponseModel.getMax().equals("0.00"));
        assertTrue(statisticsResponseModel.getMin().equals("0.00"));
        assertTrue(statisticsResponseModel.getCount() == 0L);
        assertTrue(statisticsResponseModel.getSum().equals("0.00"));
        assertTrue(statisticsResponseModel.getAvg().equals("0.00"));

    }
}
