package com.n26.model;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

public class TransactionModel {

    private UUID uuid;
    private BigDecimal amount;
    private Long timestamp;

    public TransactionModel() {
    }

    public TransactionModel(UUID uuid, BigDecimal amount, Long timestamp) {
        this.uuid = uuid;
        this.amount = amount;
        this.timestamp = timestamp;
    }

    public TransactionModel(TransactionModelRequest transactionModelRequest) {
        this.uuid = UUID.randomUUID();
        this.amount = new BigDecimal(transactionModelRequest.getAmount());
        this.timestamp =  Instant.parse(transactionModelRequest.getTimestamp()).toEpochMilli();
    }


    public TransactionModel(BigDecimal amount, Long timestamp) {
        this.amount = amount;
        this.timestamp = timestamp;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
}
