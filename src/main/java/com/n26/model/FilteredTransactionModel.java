package com.n26.model;

public class FilteredTransactionModel {
    private TransactionModel transactionModel;
    private Boolean old;

    public FilteredTransactionModel(TransactionModel transactionModel, Boolean old) {
        this.transactionModel = transactionModel;
        this.old = old;
    }

    public TransactionModel getTransactionModel() {
        return transactionModel;
    }

    public void setTransactionModel(TransactionModel transactionModel) {
        this.transactionModel = transactionModel;
    }

    public Boolean getOld() {
        return old;
    }

    public void setOld(Boolean old) {
        this.old = old;
    }
}
