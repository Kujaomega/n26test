package com.n26.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class StatisticsResponseModel {
    private String sum;
    private String avg;
    private String max;
    private String min;
    private Long count;

    public StatisticsResponseModel(StatisticsModel statisticsModel){
        String minResult = statisticsModel.getMin().get(0).getAmount().setScale(2, RoundingMode.HALF_UP).toString();
        if (statisticsModel.getMin().get(0).getAmount().compareTo(new BigDecimal(Integer.MAX_VALUE)) == 0) {
            minResult = "0.00";
        }
        this.sum = statisticsModel.getSum().setScale(2, RoundingMode.HALF_UP).toString();
        this.avg = statisticsModel.getAvg().setScale(2, RoundingMode.HALF_UP).toString();
        this.max = statisticsModel.getMax().get(0).getAmount().setScale(2, RoundingMode.HALF_UP).toString();
        this.min = minResult;
        this.count = statisticsModel.getCount();
    }
    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }

    public String getAvg() {
        return avg;
    }

    public void setAvg(String avg) {
        this.avg = avg;
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
