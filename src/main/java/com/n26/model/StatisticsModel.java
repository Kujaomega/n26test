package com.n26.model;

import com.google.common.collect.Lists;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

public class StatisticsModel {
    private BigDecimal sum;
    private BigDecimal avg;
    private List<TransactionModel> max = Lists.newArrayList();
    private List<TransactionModel> min = Lists.newArrayList();
    private Long count;

    public StatisticsModel() {
        this.sum = new BigDecimal(0);
        this.avg = new BigDecimal(0);

        this.max.add(new TransactionModel(new BigDecimal(0), Instant.now().toEpochMilli()));
        this.min.add(new TransactionModel(new BigDecimal(Integer.MAX_VALUE), Instant.now().toEpochMilli()));
        this.count = 0L;
    }

    public StatisticsModel(BigDecimal sum, BigDecimal avg, List<TransactionModel> max, List<TransactionModel> min, Long count) {
        this.sum = sum;
        this.avg = avg;
        this.max = max;
        this.min = min;
        this.count = count;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public BigDecimal getAvg() {
        return avg;
    }

    public void setAvg(BigDecimal avg) {
        this.avg = avg;
    }

    public List<TransactionModel> getMax() {
        return max;
    }

    public void setMax(List<TransactionModel> max) {
        this.max = max;
    }

    public List<TransactionModel> getMin() {
        return min;
    }

    public void setMin(List<TransactionModel> min) {
        this.min = min;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
