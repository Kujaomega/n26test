package com.n26.model;


public class TransactionModelRequest {
    private String amount;
    private String timestamp;

    public TransactionModelRequest() {
    }

    public TransactionModelRequest(String amount, String timestamp) {
        this.amount = amount;
        this.timestamp = timestamp;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
