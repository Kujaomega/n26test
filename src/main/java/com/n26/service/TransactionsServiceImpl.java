package com.n26.service;

import com.n26.exception.InvalidTimestampException;
import com.n26.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Component
public class TransactionsServiceImpl implements TransactionsService {

    private static final int THREADS = 1;
    private static final int OLD_TRANSACTIONS_TIME = 60000;
    private static final int TIME_STATISTICS = 60000;
    private static final int MAX_MIN_LIST_LENGTH = 2;

    private Logger logger = LoggerFactory.getLogger(TransactionsServiceImpl.class);

    private StatisticsModel statisticsModel = new StatisticsModel();
    private ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(THREADS);


    @Override
    public StatisticsResponseModel getStatistics() {
        return new StatisticsResponseModel(statisticsModel);
    }

    @Override
    public FilteredTransactionModel createTransaction(TransactionModelRequest transactionModel) {
        FilteredTransactionModel filteredTransactionModel = filterTransaction(transactionModel);
        if(filteredTransactionModel.getOld()){
            return filteredTransactionModel;
        }
        modifyStatistics(filteredTransactionModel.getTransactionModel(), StatisticsActions.INCREMENT);
        setFutureUpdate(filteredTransactionModel.getTransactionModel());
        return filteredTransactionModel;
    }

    @Override
    public void deleteTransactions() {
        executor.shutdownNow();
        this.statisticsModel = new StatisticsModel();
        executor = new ScheduledThreadPoolExecutor(THREADS);
    }

    private FilteredTransactionModel filterTransaction(TransactionModelRequest transactionModel){
        Boolean old = false;
        try{
            long actualTimeStamp = Instant.now().toEpochMilli();
            Instant transactionTimeInstant = Instant.parse(transactionModel.getTimestamp());
            Long transactionTime = transactionTimeInstant.toEpochMilli();
            if(transactionTime > actualTimeStamp){
                throw new InvalidTimestampException();
            }
            Long result = Math.abs(actualTimeStamp - transactionTime);
            if( result > OLD_TRANSACTIONS_TIME) {
                old = true;
            }
            return new FilteredTransactionModel(new TransactionModel(transactionModel), old);
        } catch (Exception e){
            throw new InvalidTimestampException();
        }
    }

    private void setFutureUpdate(TransactionModel transactionModel){
        long expirationTimeStamp = Instant.now().minusMillis(TIME_STATISTICS).toEpochMilli();
        long transactionMillis = transactionModel.getTimestamp();
        Runnable decrementStatistics = () -> {
            try {
                modifyStatistics(transactionModel, StatisticsActions.DECREMENT);
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        };

        executor.schedule(decrementStatistics, transactionMillis - expirationTimeStamp, TimeUnit.MILLISECONDS);

    }

    private synchronized void modifyStatistics(TransactionModel transactionModel, StatisticsActions statisticsActions){
        if(statisticsActions == StatisticsActions.INCREMENT) {
            setNewStatistics(transactionModel);
        } else if(statisticsActions == StatisticsActions.DECREMENT) {
            removeTransactionTimeStatistics(transactionModel);
        }
    }

    private void setNewStatistics(TransactionModel transactionModel){
        Long countTransactions = statisticsModel.getCount() + 1;
        BigDecimal sumTransactions = statisticsModel.getSum().add(transactionModel.getAmount());
        BigDecimal avgTransactions = sumTransactions.divide(new BigDecimal(countTransactions), 2, RoundingMode.HALF_UP)
                .setScale(2, BigDecimal.ROUND_HALF_UP);
        List<TransactionModel> maxList = setMaxTransaction(statisticsModel.getMax(), transactionModel);
        List<TransactionModel> minList = setMinTransaction(statisticsModel.getMin(), transactionModel);

        statisticsModel.setCount(countTransactions);
        statisticsModel.setSum(sumTransactions);
        statisticsModel.setAvg(avgTransactions);
        statisticsModel.setMax(maxList);
        statisticsModel.setMin(minList);
    }

    private List<TransactionModel> setMaxTransaction(List<TransactionModel> maxList, TransactionModel transactionModel){
        int transactionPositionToAdd = -1;
        for(int i=0; i < maxList.size(); ++i){
            if(maxList.get(i).getAmount().compareTo(transactionModel.getAmount()) < 0 && transactionPositionToAdd == -1){
                transactionPositionToAdd = i;
            }
        }

        return addTransactionToList(transactionModel, transactionPositionToAdd, maxList);
    }

    private List<TransactionModel> setMinTransaction(List<TransactionModel> minList, TransactionModel transactionModel){
        int transactionPositionToAdd = -1;
        for(int i=0; i < minList.size(); ++i){
            if(minList.get(i).getAmount().compareTo(transactionModel.getAmount()) > 0 && transactionPositionToAdd == -1){
                transactionPositionToAdd = i;
            }
        }

        return addTransactionToList(transactionModel, transactionPositionToAdd, minList);
    }

    private List<TransactionModel> addTransactionToList(TransactionModel transactionModel, int transactionPositionToAdd,
                                                        List<TransactionModel> transactionList){
        if(transactionPositionToAdd != -1){
            if(transactionList.size() < MAX_MIN_LIST_LENGTH){
                transactionList.add(transactionPositionToAdd, transactionModel);
            } else {
                transactionList = transactionList.subList(0,transactionPositionToAdd);
                transactionList.add(transactionPositionToAdd, transactionModel);
            }
        }

        return transactionList;
    }



    private void removeTransactionTimeStatistics(TransactionModel transactionModel) {
        statisticsModel.setCount(statisticsModel.getCount()- 1);

        if(statisticsModel.getCount() == 0L){
            statisticsModel.setAvg(new BigDecimal(0));
            statisticsModel.setSum(new BigDecimal(0));
        } else{
            statisticsModel.setSum(statisticsModel.getSum().subtract(transactionModel.getAmount()));
            statisticsModel.setAvg(statisticsModel.getSum().divide(new BigDecimal(statisticsModel.getCount()), 2, RoundingMode.HALF_UP));
        }

        removeMaxOrMinTransactionFromList(statisticsModel.getMax(), transactionModel);
        removeMaxOrMinTransactionFromList(statisticsModel.getMin(), transactionModel);
    }

    private void removeMaxOrMinTransactionFromList(List<TransactionModel> transactionList, TransactionModel transactionModel){
        int indexToRemove = -1;

        for(int i=0; i<transactionList.size(); ++i){
            if(transactionList.get(i).getUuid().equals(transactionModel.getUuid())){
                indexToRemove = i;
            }
        }

        if(indexToRemove != -1){
            transactionList.remove(indexToRemove);
        }
    }
}
