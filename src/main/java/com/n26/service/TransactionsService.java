package com.n26.service;

import com.n26.model.FilteredTransactionModel;
import com.n26.model.StatisticsResponseModel;
import com.n26.model.TransactionModelRequest;

public interface TransactionsService {
    StatisticsResponseModel getStatistics();
    FilteredTransactionModel createTransaction(TransactionModelRequest transactionModel);
    void deleteTransactions();
}
