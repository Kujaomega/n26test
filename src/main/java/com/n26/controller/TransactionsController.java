package com.n26.controller;

import com.n26.model.FilteredTransactionModel;
import com.n26.model.StatisticsResponseModel;
import com.n26.model.TransactionModelRequest;
import com.n26.service.TransactionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TransactionsController {

    @Autowired
    TransactionsService transactionsService;

    @GetMapping("/statistics")
    public ResponseEntity<StatisticsResponseModel> getStatistics(){
        StatisticsResponseModel statisticsModel = transactionsService.getStatistics();
        return new ResponseEntity<>(statisticsModel, HttpStatus.OK);
    }

    @PostMapping("/transactions")
    public ResponseEntity<?> postTransaction(@RequestBody TransactionModelRequest transactionModel){
        FilteredTransactionModel filteredTransactionModel = transactionsService.createTransaction(transactionModel);
        if(filteredTransactionModel.getOld()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping("/transactions")
    public ResponseEntity<?> deleteTransactions(){
        transactionsService.deleteTransactions();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
